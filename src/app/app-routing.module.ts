import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeShopDistributionComponent} from './layouts/employee-shop-distribution/employee-shop-distribution.component';


const routes: Routes = [
  {path: '', component: EmployeeShopDistributionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
