import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShopListItemComponent } from './elements/shop-list-item/shop-list-item.component';
import { TabsGroupComponent } from './elements/tabs/tabs-group/tabs-group.component';
import { TabComponent } from './elements/tabs/tab/tab.component';
import { EmployeeShopDistributionComponent } from './layouts/employee-shop-distribution/employee-shop-distribution.component';
import { EmployeeWorkerShopsComponent } from './elements/employee-worker-shops/employee-worker-shops.component';
import { CountPhrasePipe } from './pipes/count-phrase.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ShopListItemComponent,
    TabsGroupComponent,
    TabComponent,
    EmployeeShopDistributionComponent,
    EmployeeWorkerShopsComponent,
    CountPhrasePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
