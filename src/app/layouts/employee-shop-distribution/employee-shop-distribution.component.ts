import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ShopDto} from '../../dto/shop.dto';
import {EmployeeWithWorkerShopsDto} from '../../dto/worker-shop.dto';
import {WorkerShopsFacade} from '../../facades/worker-shops.facade';
import {EmployeeWorkerShopsComponent} from '../../elements/employee-worker-shops/employee-worker-shops.component';

@Component({
  selector: 'app-employee-shop-distribution',
  templateUrl: './employee-shop-distribution.component.html',
  styleUrls: ['./employee-shop-distribution.component.scss']
})
export class EmployeeShopDistributionComponent implements OnInit, OnDestroy {

  employees: Array<EmployeeWithWorkerShopsDto>;
  shops: Array<ShopDto>;
  selectedEmployee: EmployeeWithWorkerShopsDto;
  @ViewChild('tab_body') selectedEmployeeRef: EmployeeWorkerShopsComponent;

  constructor(private facade: WorkerShopsFacade) { }

  ngOnInit(): void {
    this.facade.getEmployeesWithWorkerShops().subscribe(data => {
      this.employees = data;
      if (this.employees.length) {
        if (this.selectedEmployee){
          this.selectEmployee(data.find(({employee}) => employee.id === this.selectedEmployee.employee.id));
        } else {
          this.selectEmployee(this.employees[0]);
        }
      }
    });
    this.facade.getUndistributedShops().subscribe(data => this.shops = data);
  }

  ngOnDestroy(): void {
    this.facade.destroy();
  }


  selectEmployee(employee: EmployeeWithWorkerShopsDto) {
    this.selectedEmployee = employee;
  }

  addEmployee(){
    this.facade.addEmployee().then(employee => {
      this.employees.push(employee);
      this.selectEmployee(employee);
    });
  }

  removeSelectedEmployee(){
    this.facade.removeEmployee(this.selectedEmployee.employee.id).then(() => {
      this.employees = this.employees.filter(({employee}) => employee.id !== this.selectedEmployee.employee.id);
      this.shops = this.shops.concat(this.selectedEmployee.workerShops);
      this.selectedEmployee = this.employees.length ? this.employees[0] : null;
    });
  }

  selectShopForEmployee(shop: ShopDto) {
    if (this.selectedEmployee) {
      this.selectedEmployee.workerShops.push(shop);
      this.selectedEmployeeRef.detectChanges();
      this.shops = this.shops.filter(s => s.id !== shop.id);
    }
  }

  unselectShopForEmployee(shop: ShopDto) {
    this.selectedEmployee.workerShops = this.selectedEmployee.workerShops.filter(s => s.id !== shop.id);
    this.shops.push(shop);
  }

  saveData(){
    if (this.employees.length) {
      this.facade.updateWorkerShops(this.employees);
    }
  }

}
