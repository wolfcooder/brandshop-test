import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ShopDto} from '../../dto/shop.dto';

@Component({
  selector: 'app-shop-list-item',
  templateUrl: './shop-list-item.component.html',
  styleUrls: ['./shop-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShopListItemComponent implements OnInit {

  @Input() data: ShopDto;
  @Input() disabled = false;

  constructor() { }

  ngOnInit(): void {
  }

}
