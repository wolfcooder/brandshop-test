import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmployeeWithWorkerShopsDto} from '../../dto/worker-shop.dto';
import {EmployeeDto} from '../../dto/employee.dto';
import {ShopDto} from '../../dto/shop.dto';

@Component({
  selector: 'app-employee-worker-shops',
  templateUrl: './employee-worker-shops.component.html',
  styleUrls: ['./employee-worker-shops.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeWorkerShopsComponent implements OnInit {

  @Input() data: EmployeeWithWorkerShopsDto;

  @Output() removeEmployee = new EventEmitter<EmployeeDto>();
  @Output() removeShop = new EventEmitter<ShopDto>();

  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  onRemoveEmployee(){
    this.removeEmployee.emit(this.data.employee);
  }

  onRemoveShop(shop: ShopDto){
    this.removeShop.emit(shop);
  }

  detectChanges(){
    this.changeDetectorRef.detectChanges()
  }

}
