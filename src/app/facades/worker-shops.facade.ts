import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {EmployeeWithWorkerShopsDto, WorkerShopDto} from '../dto/worker-shop.dto';
import {ShopDto} from '../dto/shop.dto';
import {EmployeeMockService} from '../services/mocks/employee-mock.service';
import {ShopMockService} from '../services/mocks/shop-mock.service';

@Injectable({
  providedIn: 'root'
})

export class WorkerShopsFacade {

  private employeesWithWorkerShops: BehaviorSubject<EmployeeWithWorkerShopsDto[]>;
  private undistributedShops: BehaviorSubject<ShopDto[]>;

  constructor(
    private employeeService: EmployeeMockService,
    private shopService: ShopMockService
  ){
    this.employeesWithWorkerShops = new BehaviorSubject<EmployeeWithWorkerShopsDto[]>(null);
    this.undistributedShops = new BehaviorSubject<ShopDto[]>(null);
  }

  getEmployeesWithWorkerShops(): BehaviorSubject<EmployeeWithWorkerShopsDto[]> {
    this.employeeService.getEmployeesWithWorkerShops(this.employeesWithWorkerShops);
    return this.employeesWithWorkerShops;
  }

  getUndistributedShops(): BehaviorSubject<ShopDto[]> {
    this.shopService.getUndistributedShops(this.undistributedShops);
    return this.undistributedShops;
  }

  addEmployee(): Promise <EmployeeWithWorkerShopsDto> {
    return new Promise <EmployeeWithWorkerShopsDto> ((resolve, reject) => {
      this.employeeService.addEmployeeFromStaticStorage()
        .then(employee => resolve({employee, workerShops: []}),
          error => reject(error));
    });
  }

  removeEmployee(employeeId: number): Promise<any>{
    return this.employeeService.moveEmployeeToStaticStorage(employeeId);
  }

  updateWorkerShops(data: EmployeeWithWorkerShopsDto[]) {
    const request = new Array<WorkerShopDto>();
    data.forEach(employee =>
      employee.workerShops.forEach(shop => {
        request.push({employeeId: employee.employee.id, shopId: shop.id});
    }));
    this.employeeService.updateWorkerShops(request).then(() => {
      this.employeeService.getEmployeesWithWorkerShops(this.employeesWithWorkerShops);
    });
  }

  destroy() {
    this.undistributedShops.complete();
    this.employeesWithWorkerShops.complete();
  }

}
