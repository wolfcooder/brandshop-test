import {Injectable} from '@angular/core';
import {EmployeeWithWorkerShopsDto, WorkerShopDto} from '../../dto/worker-shop.dto';
import {EmployeeDto} from '../../dto/employee.dto';
import {BehaviorSubject} from 'rxjs';
import {MockStorage} from './storage/mock.storage';

@Injectable({
  providedIn: 'root'
})

export class EmployeeMockService {

  constructor(private storage: MockStorage) {
  }

  getAllEmployees(subject: BehaviorSubject<EmployeeDto[]>) {
    subject.next(this.storage.employees);
  }

  getEmployeesWithWorkerShops(subject: BehaviorSubject<EmployeeWithWorkerShopsDto[]>) {
    const result: EmployeeWithWorkerShopsDto[] = this.storage.employees.map(employee => {
      const workerShops =  this.storage.workerShops.filter(ws => ws.employeeId === employee.id);
      return {employee, workerShops: workerShops.map(
        ws => this.storage.shops.find(
          shop => shop.id === ws.shopId
        ))};
    });
    subject.next(result);
  }

  addEmployeeFromStaticStorage(): Promise<EmployeeDto> {
    return new Promise<EmployeeDto>((resolve, reject) => {
      if (this.storage.employeesStatic.length) {
        const employee = this.storage.employeesStatic[0];
        this.storage.employeesStatic.shift();
        this.storage.employees.push(employee);
        resolve(employee);
      } else {
        reject('Static employee storage is empty!');
      }
    });
  }

  moveEmployeeToStaticStorage(employeeId: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const employee = this.storage.employees.find(e => e.id === employeeId);
      if (employee) {
        this.storage.employeesStatic.push(employee);
        this.storage.employees = this.storage.employees.filter(e => e.id !== employeeId);
        resolve();
      } else {
        reject('Employee not found!');
      }
    });
  }

  updateWorkerShops(workerShops: WorkerShopDto[]): Promise<any> {
    return new Promise<any>(resolve => {
      this.storage.workerShops = workerShops;
      console.log('Mock CreateWorkerShopRequest:: ', workerShops);
      resolve();
    });
  }
}
