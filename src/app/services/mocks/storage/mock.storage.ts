import {Injectable} from '@angular/core';
import * as EmployeesStatic from './employees.json';
import * as ShopsStatic from './shops.json';
import {EmployeeDto} from '../../../dto/employee.dto';
import {ShopDto} from '../../../dto/shop.dto';
import {WorkerShopDto} from '../../../dto/worker-shop.dto';

@Injectable({
  providedIn: 'root'
})

export class MockStorage {

  employeesStatic: Array<EmployeeDto>;

  employees: Array<EmployeeDto> = new Array<EmployeeDto>();
  shops: Array<ShopDto> = new Array<ShopDto>();

  workerShops: Array<WorkerShopDto> = new Array<WorkerShopDto>();

  constructor() {
    this.employeesStatic = EmployeesStatic.data;
    this.shops = ShopsStatic.data;
  }
}
