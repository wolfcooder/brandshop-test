import {Injectable} from '@angular/core';
import {MockStorage} from './storage/mock.storage';
import {BehaviorSubject} from 'rxjs';
import {ShopDto} from '../../dto/shop.dto';

@Injectable({
  providedIn: 'root'
})

export class ShopMockService {
  constructor(private storage: MockStorage){ }

  getAllShops(subject: BehaviorSubject<ShopDto[]>) {
    subject.next(this.storage.shops);
  }

  getUndistributedShops(subject: BehaviorSubject<ShopDto[]>){
    subject.next(
      this.storage.shops.filter(
      shop => !this.storage.workerShops.some(ws => ws.shopId === shop.id)
    ));
  }
}
