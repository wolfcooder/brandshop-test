export interface ShopDto {
  id: number;
  name: string;
  fullAddress: string;
}
