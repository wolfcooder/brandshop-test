import {EmployeeDto} from './employee.dto';
import {ShopDto} from './shop.dto';

export interface WorkerShopDto {
  employeeId: number;
  shopId: number;
}


export interface EmployeeWithWorkerShopsDto {
  employee: EmployeeDto;
  workerShops: ShopDto[];
}

