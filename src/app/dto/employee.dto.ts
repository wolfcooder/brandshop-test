export interface EmployeeDto {
  id: number;
  fullName: string;
  logo: string;
}
